'use strict';

/**
 * aforo router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::aforo.aforo');
