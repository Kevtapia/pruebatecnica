'use strict';

/**
 *  aforo controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::aforo.aforo');
