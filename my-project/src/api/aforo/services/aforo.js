'use strict';

/**
 * aforo service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aforo.aforo');
